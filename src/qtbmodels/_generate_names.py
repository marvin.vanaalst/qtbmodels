from __future__ import annotations

from dataclasses import dataclass


@dataclass
class Name:
    abbrev: str | None = None
    description: str | None = None


PARS = {
    "a0": Name("A0", "Photosystem II reaction center 0"),
    "a1": Name("A1", "Photosystem II reaction center 1"),
    "a2": Name("A2", "Photosystem II reaction center 2"),
    "b0": Name("B0"),
    "b1": Name("B1"),
    "b2": Name("B2"),
    "b3": Name("B3"),
    "atp": Name("ATP"),
    "adp": Name("ADP"),
    "amp": Name("AMP"),
    "nadp": Name("NADP"),
    "nadph": Name("NADPH"),
    "nad": Name("NAD"),
    "nadh": Name("NADH"),
    "ps2cs": Name("PSII_cross_section"),
    "h": Name("protons"),
    "ph": Name("pH"),
    "pq_ox": Name("Plastoquinone (oxidised)"),
    "pq_red": Name("Plastoquinone (reduced)"),
    "pc_ox": Name("Plastocyanine (oxidised)"),
    "pc_red": Name("Plastocyanine (reduced)"),
    "fd_ox": Name("Ferredoxine (oxidised)"),
    "fd_red": Name("Ferredoxine (reduced)"),
    "lhc": Name("Light-harvesting complex"),
    "lhcp": Name("Light-harvesting complex (protonated)"),
    "psbs_de": Name("PsbS (de-protonated)"),
    "psbs_pr": Name("PsbS (protonated)"),
    "vx": Name("Violaxanthin"),
    "zx": Name("Zeaxanthin"),
    "tr_ox": Name("Thioredoxin (oxidised)"),
    "tr_red": Name("Thioredoxin (reduced)"),
    "pi": Name("Orthophosphate"),
    "ppi": Name("Diphosphate"),
    "co2": Name("CO2 (dissolved)"),
    "co2_atmosphere": Name("CO2 (atmosphere)"),
    "hco3": Name("HCO3"),
    "o2": Name("O2 (dissolved)"),
    "o2_atmosphere": Name("O2 (atmosphere)"),
    "h2o2": Name("H2O2"),
    "pga": Name("PGA", "3-Phosphoglycerate"),
    "pgo": Name("PGO", "2-Phosphoglycolate"),
    "bpga": Name("BPGA"),
    "gap": Name("GAP"),
    "dhap": Name("DHAP"),
    "fbp": Name("FBP"),
    "f6p": Name("F6P"),
    "g6p": Name("G6P"),
    "g1p": Name("G1P"),
    "sbp": Name("SBP"),
    "s7p": Name("S7P"),
    "erythrose": Name("erythrose"),
    "e4p": Name("E4P", "Erythrose-4-phosphate"),
    "erythrulose": Name("erythrulose"),
    "erythrulose_1p": Name("erythrulose_1p"),
    "erythrulose_4p": Name("erythrulose_4p"),
    "xylulose": Name("xylulose"),
    "x5p": Name("X5P", "Xylulose-5-phosphate"),
    "ribose": Name("ribose"),
    "r1p": Name("R1P", "Ribose-1-phosphate"),
    "r5p": Name("R5P", "Ribose-5-phosphate"),
    "ribulose": Name("ribulose"),
    "ru5p": Name("RU5P", "ribulose-5-phosphate"),
    "rubp": Name("RUBP", "ribulose-1,5-bisphosphate"),
    "starch": Name("starch"),
    "mda": Name("MDA", "monodehydroascorbate"),
    "dha": Name("DHA", "dehydroascorbate"),
    "ascorbate": Name("ascorbate"),
    "glutathion_red": Name("GSH", "glutathion (reduced)"),
    "glutathion_ox": Name("GSSG", "glutathion (oxidised)"),
    "gly": Name("glycine"),
    "glyoxylate": Name("glyoxylate"),
    "glycolate": Name("glycolate"),
    "glycerate": Name("glycerate"),
    "pyruvate": Name("pyruvate"),
    "hydroxypyruvate": Name("hydroxypyruvate"),
    "ser": Name("serine"),
    "pfd": Name("PPFD", "Photosynthetic Photon Flux Density"),
    "quencher": Name("Q"),
    "fluorescence": Name("fluorescence"),
    "e_active": Name("E_active"),
    "e_inactive": Name("E_inactive"),
    "oxoglutarate": Name("2-oxoglutarate"),
    "glutamate": Name("glutamate"),
    "nh4": Name("NH4+", "Ammonium"),
    "nh3": Name("NH3", "Ammonia"),
    "acetoacetate": Name("acetoacetate", "ketobutyrate"),
    "coa": Name("CoA", "Coenzyme-A"),
    "acetyl_coa": Name("acetyl-CoA"),
    "acetoacetyl_coa": Name("acetoacetyl-CoA"),
    "malonyl_coa": Name("malonyl-CoA"),
    "formyl_coa": Name("formyl-CoA"),
    "tartronyl_coa": Name("tartronyl-CoA"),
    "succinyl_coa": Name("succinyl-CoA"),
    "glycolyl_coa": Name("glycolyl-CoA"),
    "malonate_s_aldehyde": Name("malonate-s-aldehyde"),
    "succinate": Name("succinate"),
    "formate": Name("formate"),
    "thf": Name("THF"),
    "formyl_thf": Name("10-formyl-THF"),
    "methylene_thf": Name("methylene-THF"),
    "methenyl_thf": Name("methenyl-THF"),
    "aspartate": Name("aspartate"),
    "hydroxyaspartate": Name("hydroxyaspartate"),
    "iminoaspartate": Name("iminoaspartate"),
    "oxalate": Name("oxalate", "ethanedoic acid, oxalic acid"),
    "oxaloacetate": Name("oxaloacetate"),
    "malate": Name("malate"),
    "pep": Name("PEP", "Phosphoenolpyruvate"),
    "tartronate_semialdehyde": Name("tartronate_semialdehyde"),
    "arabinose_5_phosphate": Name("A5P"),
    "glycolaldehyde": Name("glycolaldehyde"),
    # "": Name(""),
}
