# qtbmodels

[![Documentation](https://img.shields.io/badge/Documentation-Gitlab-success)](https://marvin.vanaalst.gitlab.io/qtbmodels/)


## Models

| Name                 | Description                                                                 |
| -------------------- | --------------------------------------------------------------------------- |
| Ebenhöh 2011         | PSII & two-state quencher & ATP synthase                                    |
| Ebenhöh 2014         | PETC & state transitions & ATP synthase from Ebenhoeh 2011                  |
| Matuszyńska 2016 NPQ | 2011 + PSII & four-state quencher                                           |
| Matuszyńska 2016 PhD | ?                                                                           |
| Matuszyńska 2019     | Merges PETC (Ebenhöh 2014), NPQ (Matuszynska 2016) and CBB (Poolman 2000)   |
| Saadat 2021          | 2019 + Mehler (Valero ?) & Thioredoxin & extendend PSI states & consumption |
| van Aalst 2023       | Saadat 2021 & Yokota 1985 & Witzel 2010                                     |


## References

| Name         | Description                                           |
| ------------ | ----------------------------------------------------- |
| Poolman 2000 | CBB cycle, based on Pettersson & Ryde-Pettersson 1988 |
| Yokota 1985  | Photorespiration                                      |
| Valero ?     |                                                       |

