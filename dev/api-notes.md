# API notes

## Failed ideas

### `require` decorator

```python
from typing import Any, Callable, TypeVar, cast

ModelFn = TypeVar("ModelFn", bound=Callable)

def require(components: list[str]):
    """
    Usage
    -----

    @require(["ATP", "ADP", "NADPH", "NADP"])
    def get_cbb():
        ...
    """

    def inner(fn: ModelFn) -> ModelFn:
        def wrapper(model: Model, *args: Any, **kwargs: Any) -> Model:
            for component in components:
                if component not in model._ids:
                    raise ValueError(f"Missing component {component}")

            return cast(Model, fn(*args, **kwargs))

        return cast(ModelFn, wrapper)

    return inner
```

This one looked great, but failes once comparments are important.
E.g in the example below, `chl_stroma` isn't known at the time the decorator is defined.

```python

@require([n.atp(), n.adp(), n.nadph(), n.nadp(), n.h(chl_stroma), n.co2()])
def add_cbb(
    model: Model,
    chl_stroma: str,
    static_co2: bool,
    static_nadph: bool,
    static_protons: bool,
) -> Model:
```

Replaced this now with a simple fn call, which sadly isn't as satisfying

```python
def require(model: Model, components: list[str]) -> None:
    """
    Usage
    -----

    def get_cbb():
        require(["ATP", "ADP", ...])
        ...
    """
    for component in components:
        if component not in model._ids:
            raise ValueError(f"Missing component {component}")
    return None


def add_cbb(
    model: Model,
    chl_stroma: str,
    static_co2: bool,
    static_nadph: bool,
    static_protons: bool,
) -> Model:
    require(
        model,
        [
            n.atp(chl_stroma),
            n.adp(chl_stroma),
            n.nadph(chl_stroma),
            n.nadp(chl_stroma),
            n.h(chl_stroma),
            n.co2(chl_stroma),
        ],
    )
```
