import numpy as np
import pandas as pd
from qtbmodels.v1 import Simulator, get_ebenhoeh2014, get_y0_ebenhoeh2014


def test_missing_parameters() -> None:
    m = get_ebenhoeh2014()
    assert not m.check_missing_parameters()


def test_unused_compounds() -> None:
    m = get_ebenhoeh2014()
    assert not m.check_unused_compounds()


def test_unused_parameters() -> None:
    m = get_ebenhoeh2014()
    assert not m.check_unused_parameters()


def test_stable_integration() -> None:
    assert np.allclose(
        pd.Series(get_y0_ebenhoeh2014()),
        pd.Series(
            Simulator(get_ebenhoeh2014())
            .initialise(get_y0_ebenhoeh2014())
            .simulate_and(100_000)
            .get_new_y0()
        ),
    )
