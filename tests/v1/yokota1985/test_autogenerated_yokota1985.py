from qtbmodels.v1 import get_yokota1985


def test_missing_parameters() -> None:
    m = get_yokota1985(static_co2=True)
    assert not m.check_missing_parameters()


def test_unused_compounds() -> None:
    m = get_yokota1985(static_co2=True)
    assert not m.check_unused_compounds()


def test_unused_parameters() -> None:
    m = get_yokota1985(static_co2=True)
    assert not m.check_unused_parameters()
