import numpy as np
import pandas as pd
from qtbmodels.v1 import Simulator, get_matuszynska2019, get_y0_matuszynska2019


def test_missing_parameters() -> None:
    m = get_matuszynska2019(static_co2=True)
    assert not m.check_missing_parameters()


def test_unused_compounds() -> None:
    m = get_matuszynska2019(static_co2=True)
    assert not m.check_unused_compounds()


def test_unused_parameters() -> None:
    m = get_matuszynska2019(static_co2=True)
    assert not m.check_unused_parameters()


def test_stable_integration() -> None:
    assert np.allclose(
        pd.Series(get_y0_matuszynska2019()).sort_index(),
        pd.Series(
            Simulator(get_matuszynska2019(static_co2=True))
            .initialise(get_y0_matuszynska2019())
            .simulate_and(100_000)
            .get_new_y0()
        ).sort_index(),
    )
