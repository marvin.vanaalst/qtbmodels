from typing import Any

import numpy as np
import pandas as pd


def _template(
    get_old: Any, get_new: Any, get_y0_old: Any, get_y0_new: Any
) -> None:
    assert (
        np.linalg.norm(
            (
                pd.DataFrame(
                    get_old().get_full_concentration_dict(get_y0_old())
                )
                - pd.DataFrame(
                    get_new().get_full_concentration_dict(get_y0_new())
                )
            )
            .loc[0]
            .dropna(),
            ord=2,
        )
        < 1e-6
    )

    assert (
        np.linalg.norm(
            (
                get_old().get_fluxes_df(get_y0_old()).loc[0]
                - get_new().get_fluxes_df(get_y0_new()).loc[0]
            ).dropna(),
            ord=2,
        )
        < 1e-6
    )

    assert (
        np.linalg.norm(
            (
                pd.Series(get_old().get_right_hand_side(get_y0_old()))
                - pd.Series(get_new().get_right_hand_side(get_y0_new()))
            ).dropna(),
            ord=2,
        )
    ) < 1e-6


def test_yokota1985() -> None:
    from qtbmodels.v1 import get_y0_yokota1985 as get_y0_old
    from qtbmodels.v1 import get_yokota1985 as get_old
    from qtbmodels.v2 import get_y0_yokota1985 as get_y0_new
    from qtbmodels.v2 import get_yokota1985 as get_new

    _template(get_old, get_new, get_y0_old, get_y0_new)


def test_poolman2000() -> None:
    from qtbmodels.v1 import get_poolman2000 as get_old
    from qtbmodels.v1 import get_y0_poolman2000 as get_y0_old
    from qtbmodels.v2 import get_poolman2000 as get_new
    from qtbmodels.v2 import get_y0_poolman2000 as get_y0_new

    _template(get_old, get_new, get_y0_old, get_y0_new)


def test_matuszynska2016npq() -> None:
    from qtbmodels.v1 import get_matuszynska2016npq as get_old
    from qtbmodels.v1 import get_y0_matuszynska2016npq as get_y0_old
    from qtbmodels.v2 import get_matuszynska2016npq as get_new
    from qtbmodels.v2 import get_y0_matuszynska2016npq as get_y0_new

    _template(get_old, get_new, get_y0_old, get_y0_new)


def test_matuszynska2019() -> None:
    from qtbmodels.v1 import get_matuszynska2019 as get_old
    from qtbmodels.v1 import get_y0_matuszynska2019 as get_y0_old
    from qtbmodels.v2 import get_matuszynska2019 as get_new
    from qtbmodels.v2 import get_y0_matuszynska2019 as get_y0_new

    _template(get_old, get_new, get_y0_old, get_y0_new)


def test_saadat2021() -> None:
    from qtbmodels.v1 import get_saadat2021 as get_old
    from qtbmodels.v1 import get_y0_saadat2021 as get_y0_old
    from qtbmodels.v2 import get_saadat2021 as get_new
    from qtbmodels.v2 import get_y0_saadat2021 as get_y0_new

    _template(get_old, get_new, get_y0_old, get_y0_new)
