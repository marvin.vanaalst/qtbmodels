from qtbmodels.v2 import get_saadat2021


def test_missing_parameters() -> None:
    m = get_saadat2021(static_co2=True)
    assert not m.check_missing_parameters()


def test_unused_compounds() -> None:
    m = get_saadat2021(static_co2=True)
    assert not m.check_unused_compounds()


def test_unused_parameters() -> None:
    m = get_saadat2021(static_co2=True)
    assert not m.check_unused_parameters()
